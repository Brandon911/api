let url = 'http://jsonplaceholder.typicode.com/users'
        fetch(url)
        .then(response => response.json())
        .then(data => mostrarData(data))
        .then(error => console.log(error))

const mostrarData = (data) => {
    console.log(data)
    let body = ''
    for (let i = 0; i < data.length; i++){      
        body += `<tr>
                <td>${data[i].id}</td>
                <td>${data[i].name}</td>
                <td>${data[i].email}</td>
                </tr>`
    }

    document.getElementById('data').innerHTML = body
} 
/*
function ajax_get_json(usuarioIngresado){
    var resultado = document.getElementById("info");
    var xmlhttp;

    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp =   ActiveXObject("Microsoft.XMLHTTP");
    }
    if (usuarioIngresado.length === 0) {
        resultado.innerHTML = "";
    } else {
        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                var datos = JSON.parse(xmlhttp.resposeText);

                var x = encontrarPersona(datos, usuarioIngresado);
                var mensaje = (x === true) ? "<span class = 'encontrado'>Si fue encontrado</span>":
                                            "<span class = 'no-encontrado'>No fue encontrado</span>";
                resultado.innerHTML = mensaje;
            
            }
        }
        xmlhttp.open("GET", "datos.json", true);
        xmlhttp.send();
    }
}

function encontrado(objetoJSON, usuario) {
    var arreglo = [];

    for(var i in objetoJSON){
        var persona = objetoJSON[i];
        arreglo.push(persona.name);
    }
    return arreglo.indexOf(usuario) > -1;
}
*/